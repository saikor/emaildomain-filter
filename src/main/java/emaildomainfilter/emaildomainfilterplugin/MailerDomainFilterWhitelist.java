package emaildomainfilter.emaildomainfilterplugin;
import hudson.Launcher;
import hudson.Extension;
import hudson.FilePath;
import hudson.util.FormValidation;
import hudson.model.AbstractBuild;
import hudson.model.AbstractProject;
import hudson.model.BuildListener;
import hudson.model.Run;
import hudson.model.TaskListener;
import hudson.tasks.Builder;
import hudson.tasks.BuildStepDescriptor;
import jenkins.plugins.mailer.tasks.MailAddressFilter;
import jenkins.plugins.mailer.tasks.MailAddressFilterDescriptor;
import jenkins.tasks.SimpleBuildStep;
import net.sf.json.JSONObject;
import org.kohsuke.stapler.DataBoundConstructor;
import org.kohsuke.stapler.StaplerRequest;
import org.kohsuke.stapler.QueryParameter;

import javax.mail.internet.InternetAddress;
import javax.servlet.ServletException;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Sample {@link Builder}.
 *
 * <p>
 * When the user configures the project and enables this builder,
 * {@link DescriptorImpl#newInstance(StaplerRequest)} is invoked
 * and a new {@link MailerDomainFilterWhitelist} is created. The created
 * instance is persisted to the project configuration XML by using
 * XStream, so this allows you to use instance fields (like {@link #name})
 * to remember the configuration.
 *
 * <p>
 * When a build is performed, the {@link #perform} method will be invoked. 
 *
 * @author Kohsuke Kawaguchi
 */
@Extension
public class MailerDomainFilterWhitelist extends MailAddressFilter {


    /**
     * Descriptor for {@link MailerDomainFilterWhitelist}. Used as a singleton.
     * The class is marked as public so that it can be accessed from views.
     *
     * <p>
     * See <tt>src/main/resources/hudson/plugins/hello_world/HelloWorldBuilder/*.jelly</tt>
     * for the actual HTML fragment for the configuration screen.
     */
    @Extension // This indicates to Jenkins that this is an implementation of an extension point.
    public static final class DescriptorImpl extends MailAddressFilterDescriptor {
        /**
         * To persist global configuration information,
         * simply store it in a field and call save().
         *
         * <p>
         * If you don't want fields to be persisted, use <tt>transient</tt>.
         */
        private boolean restrictDomains;
        private String restrictedDomainList;

        /**
         * In order to load the persisted global configuration, you have to 
         * call load() in the constructor.
         */
        public DescriptorImpl() {
            load();
        }

        /**
         * This human readable name is used in the configuration screen.
         */
        public String getDisplayName() {
            return "Say hello world";
        }

        @Override
        public boolean configure(StaplerRequest req, JSONObject formData) throws FormException {
            // To persist global configuration information,
            // set that to properties and call save().
            restrictDomains = formData.getBoolean("restrictDomains");
            restrictedDomainList = formData.getString("restrictedDomainList");
            // ^Can also use req.bindJSON(this, formData);
            //  (easier when there are many fields; need set* methods for this, like setUseFrench)
            save();
            return super.configure(req,formData);
        }

        /**
         * This method returns true if the global configuration says we should speak French.
         *
         * The method name is bit awkward because global.jelly calls this method to determine
         * the initial state of the checkbox by the naming convention.
         */
        public boolean getRestrictDomains() {
            return restrictDomains;
        }
        public String getRestrictedDomainList()
        {
        	return restrictedDomainList;
        }
    }
	@Override
	public boolean isFiltered(AbstractBuild<?, ?> arg0, BuildListener arg1, InternetAddress arg2) {
		// TODO Auto-generated method stub
		arg1.getLogger().println("Checking recipient "+arg2.getAddress()+" email domain against whitelist");
		String rdl = ((DescriptorImpl) getDescriptor()).getRestrictedDomainList();
		
		for(String domain:rdl.split(","))
		{
			arg1.getLogger().println("Checking domain"+domain);
			if(arg2.getAddress().contains("@"+domain))
			{
				arg1.getLogger().println("Recipient domain matched");
				return false;
			}
		}
		arg1.getLogger().println("Recipient "+arg2.getAddress()+"has an invalid domain, removing from list");
		return true;
	}
}

